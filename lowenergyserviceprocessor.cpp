#include "lowenergyserviceprocessor.h"

#include "configuration.h"
#include "qthelper.h"

#include <QLowEnergyController>
#include <QLowEnergyService>
#include <QtDebug>
#include <QTimer>

LowEnergyServiceProcessor::LowEnergyServiceProcessor(std::shared_ptr<ConfigurationEntry> pConfig, QLowEnergyController* pQLowEnergyController, QObject *parent)
    : QObject(parent)
    , m_pConfig(pConfig)
{
    m_pLowEnergyService = pQLowEnergyController->createServiceObject(m_pConfig->m_ServiceUuid, this);

    QObject::connect(m_pLowEnergyService, &QLowEnergyService::characteristicChanged,
                     m_pConfig->parser(), &CharacteristicChangeParser::parse);
    QObject::connect(m_pLowEnergyService, &QLowEnergyService::stateChanged,
                     [&](QLowEnergyService::ServiceState serviceState) {
        switch (serviceState) {
        case QLowEnergyService::DiscoveringServices:
            qInfo() << "Discovering service details...";
            break;
        case QLowEnergyService::ServiceDiscovered:
        {
            qInfo() << "Service details discovered.";
            startUsing();
            break;
        }
        case QLowEnergyService::InvalidService:
            qInfo() << "QLowEnergyService::InvalidService";
            fail();
            break;
        default:
            //nothing for now
            qInfo() << "QLowEnergyService::stateChanged: " << QtEnumToString(serviceState);
            break;
        }

    });
    QObject::connect(m_pLowEnergyService, QOverload<QLowEnergyService::ServiceError>::of(&QLowEnergyService::error),
                     [&](QLowEnergyService::ServiceError serviceError) {
            //nothing for now
            qInfo() << "QLowEnergyService::ServiceError:" << QtEnumToString(serviceError);
            fail();
    });
}

void LowEnergyServiceProcessor::startProcessing()
{
    qInfo() << "Starting processing of service. ServiceState:" << QtEnumToString(m_pLowEnergyService->state());
    switch (m_pLowEnergyService->state()) {
    case QLowEnergyService::ServiceState::DiscoveryRequired: {
        m_pLowEnergyService->discoverDetails();
        break;
    }
    case QLowEnergyService::ServiceState::InvalidService: {
        fail();
        break;
    }
    case QLowEnergyService::ServiceState::ServiceDiscovered: {
        startUsing();
        break;
    }
    default:{
        // do nothing
    }
    }
}

void LowEnergyServiceProcessor::fail()
{
    QTimer::singleShot(1000, [this]() { emit serviceProcessingFailed(); });
}

void LowEnergyServiceProcessor::startUsing()
{
    qInfo() << "Trying to use configured characteristic" << m_pConfig->m_CharacteristicUuid << "...";
    const QLowEnergyCharacteristic characteristic = m_pLowEnergyService->characteristic(m_pConfig->m_CharacteristicUuid);
    if (!characteristic.isValid()) {
        qInfo() << "Configured characteristic " << m_pConfig->m_CharacteristicUuid << " not found.";
        fail();
        return;
    }

    // if notifications supported by the characteristic
    if (characteristic.properties().testFlag(QLowEnergyCharacteristic::Notify) &&
            characteristic.descriptor(QBluetoothUuid::ClientCharacteristicConfiguration).isValid()) {

        // enable change notifications for the characteristic
        qInfo() << "Enabling notifications for characteristic" << characteristic.uuid() << "...";
        auto descriptor = characteristic.descriptor(QBluetoothUuid::ClientCharacteristicConfiguration);
        m_pLowEnergyService->writeDescriptor(descriptor, QByteArray::fromHex("0100"));
    }

    QTimer::singleShot(1000, [=]() {
        if (!m_pConfig->parser()->prepare(m_pLowEnergyService)) {
            fail();
        }
    });
}
