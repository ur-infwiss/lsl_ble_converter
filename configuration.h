#pragma once

#include "characteristicchangeparser.h"

#include <memory>
#include <QString>
#include <QBluetoothAddress>
#include <QBluetoothUuid>

class LowEnergyDeviceProcessor;
class LowEnergyServiceProcessor;
class QLowEnergyService;
class QSettings;

struct ConfigurationEntry: std::enable_shared_from_this<ConfigurationEntry> {
    explicit ConfigurationEntry() = default;
    ConfigurationEntry(const ConfigurationEntry&) = delete;
    ConfigurationEntry& operator=(const ConfigurationEntry&) = delete;

    QString m_ConfiguredName;
    QBluetoothAddress m_MacAddress;
    QBluetoothUuid m_ServiceUuid;
    QBluetoothUuid m_CharacteristicUuid;
    QString m_DeviceName;

    LowEnergyDeviceProcessor* m_pLowEnergyDeviceProcessor = nullptr;
    LowEnergyServiceProcessor* m_pLowEnergyServiceProcessor = nullptr;

    CharacteristicChangeParser* parser();

    static QBluetoothUuid characteristicHeartRateMeasurement();
    static QBluetoothUuid characteristicBitalinoCommands();
    static QBluetoothUuid characteristicBitalinoDataPackets();

private:
    std::shared_ptr<CharacteristicChangeParser> m_pCharacteristicChangeParser = nullptr;
};

std::vector<std::shared_ptr<ConfigurationEntry>> readConfigurationEntries(QSettings& settings);
