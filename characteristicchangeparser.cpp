#include "characteristicchangeparser.h"

#include "configuration.h"

#include "lsl_cpp.h"
#include <QtEndian>
#include <QLowEnergyService>
#include <QLowEnergyCharacteristic>
#include <QtDebug>

CharacteristicChangeParser::CharacteristicChangeParser(std::shared_ptr<ConfigurationEntry> pConfig, QObject *parent)
    : QObject(parent)
    , m_pConfig(pConfig)
{
    if (m_pConfig->m_CharacteristicUuid == ConfigurationEntry::characteristicHeartRateMeasurement())
    {
        {
            lsl::stream_info info(
                        (m_pConfig->m_ConfiguredName + " - HR").toStdString(),
                        "HeartRateMeasurement",
                        3,
                        lsl::IRREGULAR_RATE,
                        lsl::channel_format_t::cf_float32,
                        ("HR_" + m_pConfig->m_MacAddress.toString()).toStdString());
            lsl::xml_element desc_channels = info.desc().append_child("channels");
            auto channel_hr = desc_channels.append_child("channel");
            channel_hr.append_child_value("label", "Heart Rate (smoothed)");
            channel_hr.append_child_value("unit", "beats per minute");
            channel_hr.append_child_value("type", "HeartRate");
            auto channel_sc = desc_channels.append_child("channel");
            channel_sc.append_child_value("label", "Sensor Contact");
            channel_sc.append_child_value("type", "bool");
            auto channel_ee = desc_channels.append_child("channel");
            channel_ee.append_child_value("label", "Energy Expenditure");
            channel_ee.append_child_value("unit", "joules");
            info.desc().append_child_value("device", m_pConfig->m_DeviceName.toStdString());
            m_pLSLOutletHeartrate = std::make_shared<lsl::stream_outlet>(info);
        }
        {
            lsl::stream_info info(
                        (m_pConfig->m_ConfiguredName + " - RR").toStdString(),
                        "RR-Intervals",
                        1,
                        lsl::IRREGULAR_RATE,
                        lsl::channel_format_t::cf_float32,
                        ("RR_" + m_pConfig->m_MacAddress.toString()).toStdString());
            lsl::xml_element desc_channels = info.desc().append_child("channels");
            auto channel_hr = desc_channels.append_child("channel");
            channel_hr.append_child_value("label", "RR-Intervals");
            channel_hr.append_child_value("unit", "seconds");
            channel_hr.append_child_value("type", "RR-Interval");
            info.desc().append_child_value("device", m_pConfig->m_DeviceName.toStdString());
            m_pLSLOutletRRIntervals = std::make_shared<lsl::stream_outlet>(info);
        }
    } else if (m_pConfig->m_CharacteristicUuid == ConfigurationEntry::characteristicBitalinoDataPackets())
    {
        {
            lsl::stream_info info(
                        (m_pConfig->m_ConfiguredName + " - Analog Inputs").toStdString(),
                        "BitalinoAnalogInputs",
                        6,
                        10,
                        lsl::channel_format_t::cf_float32,
                        ("BA_" + m_pConfig->m_MacAddress.toString()).toStdString());
            lsl::xml_element desc_channels = info.desc().append_child("channels");
            for (int i = 0; i < 6; i++) {
                auto channel_hr = desc_channels.append_child("channel");
                channel_hr.append_child_value("label", QString("Analog Input %0").arg(i+1).toStdString());
                channel_hr.append_child_value("unit", "(unknown)");
                channel_hr.append_child_value("type", "(unknown)");
            }
            info.desc().append_child_value("device", m_pConfig->m_DeviceName.toStdString());
            m_pLSLOutletBitalinoAnalogChannels = std::make_shared<lsl::stream_outlet>(info);
        }
    }
}

bool CharacteristicChangeParser::prepare(QLowEnergyService* pLowEnergyService)
{
    if (m_pConfig->m_CharacteristicUuid == ConfigurationEntry::characteristicBitalinoDataPackets()) {
        const QLowEnergyCharacteristic characteristic = pLowEnergyService->characteristic(ConfigurationEntry::characteristicBitalinoCommands());
        if (!characteristic.isValid()) {
            qWarning() << "Characteristic " << characteristic.uuid() << " not found.";
            return false;
        }

        if (!characteristic.properties().testFlag(QLowEnergyCharacteristic::Write)) {
            qWarning() << "Characteristic " << characteristic.uuid() << "- Write not allowed.";
            return false;
        }

        qInfo() << "Enabling data acquisition on characteristic" << characteristic.uuid() << "...";
        //auto descriptor = characteristic.descriptor(QBluetoothUuid::ClientCharacteristicConfiguration);
        //pLowEnergyService->writeCharacteristic(characteristic, QByteArray::fromHex("83")); // set sample rate to 100Hz
        pLowEnergyService->writeCharacteristic(characteristic, QByteArray::fromHex("43")); // set sample rate to 10Hz
        //pLowEnergyService->writeCharacteristic(characteristic, QByteArray::fromHex("03")); // set sample rate to 1Hz
        //pLowEnergyService->writeCharacteristic(characteristic, QByteArray::fromHex("11")); // 1 channel
        pLowEnergyService->writeCharacteristic(characteristic, QByteArray::fromHex("fd")); // 6 channels
        //pLowEnergyService->writeDescriptor(descriptor, QByteArray::fromHex("fd")); // 1 channel -> 0x11
    }

    return true;
}

void CharacteristicChangeParser::parse(const QLowEnergyCharacteristic& characteristic, const QByteArray& value)
{
    // ignore any other characteristic change -> shouldn't really happen though
    if (characteristic.uuid() == ConfigurationEntry::characteristicHeartRateMeasurement()) {
        parseHeartRateMeasurement(characteristic, value);
    } else if (characteristic.uuid() == ConfigurationEntry::characteristicBitalinoDataPackets()) {
        parseBitalinoDataOrStatusPacket(characteristic, value);
    } else {
        qInfo() << "Fatal error: Cannot handle characteristic type: " << characteristic.uuid().toString();
    }
}

void CharacteristicChangeParser::parseHeartRateMeasurement(const QLowEnergyCharacteristic& characteristic, const QByteArray& value)
{
    const quint8* bytes = reinterpret_cast<const quint8*>(value.constData());

    // parse heart_rate_measurement data frame - specification:
    // https://www.bluetooth.com/specifications/gatt/viewer?attributeXmlFile=org.bluetooth.characteristic.heart_rate_measurement.xml&u=org.bluetooth.characteristic.heart_rate_measurement.xml

    //first byte of heart rate record denotes flags
    quint8 flags = bytes[0];
    bool heartRateIs16Bit         = (flags & (1 << 0)) != 0; // unit: BPM
    bool sensorContactSupported   = (flags & (1 << 1)) != 0;
    bool sensorContactDetected    = (flags & (1 << 2)) != 0;
    bool energyExpenditurePresent = (flags & (1 << 3)) != 0; // unit: kiloJoules
    bool rrIntervalsPresent       = (flags & (1 << 4)) != 0; // unit:

    std::vector<float> sample{ 0, 0, 0 };
    int offset = 1;

    sample[0] = heartRateIs16Bit
            ? qFromLittleEndian<quint16>(&(bytes[offset]))
            : bytes[offset];
    offset += heartRateIs16Bit ? 2 : 1;
    qInfo() << "Heart rate (BPM): " << sample[0];

    if (sensorContactSupported) {
        sample[1] = sensorContactDetected;
        qInfo() << "Sensor contact detected: " << sensorContactDetected;
    }

    if (energyExpenditurePresent) {
        sample[2] = qFromLittleEndian<quint16>(&(bytes[offset]));
        qInfo() << "Energy Expenditure (kJoule): " << sample[2];
        offset += 2;
    }

    m_pLSLOutletHeartrate->push_sample(sample);

    if (rrIntervalsPresent) {
        int numIntervals = (value.size() - offset) / 2;
        for (int i = 0; i < numIntervals; i++) {
            int rrRaw = qFromLittleEndian<quint16>(&(bytes[offset]));
            float rr = rrRaw / 1024.0f;
            qInfo() << "RR interval (s): " << rr;
            offset += 2;

            m_pLSLOutletRRIntervals->push_sample(std::vector<float>{ rr });
        }
    }
}

/// A frame returned by BITalino::read()
struct Frame
{
   /// %Frame sequence number (0...15).
   /// This number is incremented by 1 on each consecutive frame, and it overflows to 0 after 15 (it is a 4-bit number).
   /// This number can be used to detect if frames were dropped while transmitting data.
   char  seq;

   /// Array of digital ports states (false for low level or true for high level).
   /// On original %BITalino, the array contents are: I1 I2 I3 I4.
   /// On %BITalino 2, the array contents are: I1 I2 O1 O2.
   bool  digital[4];

   /// Array of analog inputs values (0...1023 on the first 4 channels and 0...63 on the remaining channels)
   short analog[6];
};

/// Current device state returned by BITalino::state()
struct State
{
   int   analog[6],     ///< Array of analog inputs values (0...1023)
         battery,       ///< Battery voltage value (0...1023)
         batThreshold;  ///< Low-battery LED threshold (last value set with BITalino::battery())
   /// Array of digital ports states (false for low level or true for high level).
   /// The array contents are: I1 I2 O1 O2.
   bool  digital[4];
};

// CRC4 check function

static const unsigned char CRC4tab[16] = {0, 3, 6, 5, 12, 15, 10, 9, 11, 8, 13, 14, 7, 4, 1, 2};

static bool checkCRC4(const unsigned char *data, int len)
{
   unsigned char crc = 0;

   for (int i = 0; i < len-1; i++)
   {
      const unsigned char b = data[i];
      crc = CRC4tab[crc] ^ (b >> 4);
      crc = CRC4tab[crc] ^ (b & 0x0F);
   }

   // CRC for last byte
   crc = CRC4tab[crc] ^ (data[len-1] >> 4);
   crc = CRC4tab[crc];

   return (crc == (data[len-1] & 0x0F));
}

void CharacteristicChangeParser::parseBitalinoDataOrStatusPacket(const QLowEnergyCharacteristic &characteristic, const QByteArray &value)
{
    // man kann nicht 1:1 von der Größe des Datenpakets auf die enthaltene
    // Anzahl Analogkanäle schließen -> muss i.a. manuell festgelegt werden
    unsigned char nChannels = 6;
    std::vector<float> sample(nChannels, 0);

    //qInfo() << "BLECharacteristicChanged:" << value.size() << "bytes";

    //unsigned char buffer[8]; // frame maximum size is 8 bytes
    const quint8* buffer = reinterpret_cast<const quint8*>(value.constData());

    unsigned char nBytes = nChannels + 2;
    if (nChannels >= 3 && nChannels <= 5)  nBytes++;

    if (value.size() < nBytes) {
        qWarning() << "Received packet with too small size (" << value.size() << ")";
        return;
    }

    if (!checkCRC4(buffer, nBytes))
    {
        qWarning() << "Received packet with wrong CRC4";
        return;
    }

    Frame f;
    f.seq = buffer[nBytes-1] >> 4;
    for(int i = 0; i < 4; i++) {
       f.digital[i] = ((buffer[nBytes-2] & (0x80 >> i)) != 0);
    }

    f.analog[0] = (short(buffer[nBytes-2] & 0x0F) << 6) | (buffer[nBytes-3] >> 2);
    sample[0] = f.analog[0] / 1023.0f;
    if (nChannels > 1) {
       f.analog[1] = (short(buffer[nBytes-3] & 0x03) << 8) | buffer[nBytes-4];
       sample[1] = f.analog[1] / 1023.0f;
    }
    if (nChannels > 2) {
       f.analog[2] = (short(buffer[nBytes-5]) << 2) | (buffer[nBytes-6] >> 6);
       sample[2] = f.analog[2] / 1023.0f;
    }
    if (nChannels > 3) {
       f.analog[3] = (short(buffer[nBytes-6] & 0x3F) << 4) | (buffer[nBytes-7] >> 4);
       sample[3] = f.analog[3] / 1023.0f;
    }
    if (nChannels > 4) {
       f.analog[4] = ((buffer[nBytes-7] & 0x0F) << 2) | (buffer[nBytes-8] >> 6);
       sample[4] = f.analog[4] / 63.0f;
    }
    if (nChannels > 5) {
       f.analog[5] = buffer[nBytes-8] & 0x3F;
       sample[5] = f.analog[5] / 63.0f;
    }

    m_pLSLOutletBitalinoAnalogChannels->push_sample(sample);
}
