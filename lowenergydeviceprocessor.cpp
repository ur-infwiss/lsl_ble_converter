#include "lowenergydeviceprocessor.h"

#include "configuration.h"
#include "lowenergyserviceprocessor.h"

#include <QLowEnergyController>
#include <QBluetoothLocalDevice>
#include <QtDebug>
#include "qthelper.h"

LowEnergyDeviceProcessor::LowEnergyDeviceProcessor(std::shared_ptr<ConfigurationEntry> pConfig, const QBluetoothDeviceInfo& remoteDeviceInfo, QObject *parent)
    : QObject(parent)
    , m_pConfig(pConfig)
    , m_LocalDevice()
{
    QObject::connect(&m_LocalDevice, &QBluetoothLocalDevice::pairingFinished,
                     this, &LowEnergyDeviceProcessor::startConnecting);
    QObject::connect(&m_LocalDevice, &QBluetoothLocalDevice::error,
                     [&](QBluetoothLocalDevice::Error error) {
        qWarning() << "QBluetoothLocalDevice::Error" << QtEnumToString(error);
        fail();
    });

    m_pLowEnergyController = QLowEnergyController::createCentral(remoteDeviceInfo, this);
    QObject::connect(m_pLowEnergyController, &QLowEnergyController::connected,
                     [&]() {
        qInfo() << "Connected to device " << m_pLowEnergyController->remoteName() << ". Discovering services...";
        m_pLowEnergyController->discoverServices();
    });
    QObject::connect(m_pLowEnergyController, &QLowEnergyController::discoveryFinished,
                     this, &LowEnergyDeviceProcessor::onServiceDiscoveryFinished);
    QObject::connect(m_pLowEnergyController, &QLowEnergyController::disconnected,
                     [&]() {
        qInfo() << "Disconnected from device" << m_pLowEnergyController->remoteName() << ".";
        emit deviceProcessingFailed();
    });
    QObject::connect(m_pLowEnergyController, static_cast<void (QLowEnergyController::*)(QLowEnergyController::Error)>(&QLowEnergyController::error),
                     [&](QLowEnergyController::Error error) {
        qInfo() << "QLowEnergyController::Error" << error << " - " << m_pLowEnergyController->errorString();
        fail();
    });
}

void LowEnergyDeviceProcessor::startProcessing()
{
    if (m_LocalDevice.pairingStatus(m_pConfig->m_MacAddress) == QBluetoothLocalDevice::Pairing::Unpaired) {
        m_LocalDevice.requestPairing(m_pConfig->m_MacAddress, QBluetoothLocalDevice::Pairing::AuthorizedPaired);
    } else {
        startConnecting();
    }
}

void LowEnergyDeviceProcessor::startConnecting()
{
    if (m_pLowEnergyController->state() != QLowEnergyController::ControllerState::UnconnectedState) {
        qInfo() << "Device" << m_pLowEnergyController->remoteName() << "not in Unconnected state -> disconnecting...";
        m_pLowEnergyController->disconnectFromDevice();
    } else {
        qInfo() << "Connecting to device" << m_pLowEnergyController->remoteName() << "...";
        m_pLowEnergyController->connectToDevice();
    }
}

void LowEnergyDeviceProcessor::onServiceDiscoveryFinished()
{
    qInfo() << "Service discovery finished, got" << m_pLowEnergyController->services().size() << "services.";

    if (m_pLowEnergyController->services().contains(m_pConfig->m_ServiceUuid)) {
        qInfo() << "Configured service" << m_pConfig->m_ServiceUuid.toString() << "found.";

        if (m_pConfig->m_pLowEnergyServiceProcessor == nullptr) {
            m_pConfig->m_pLowEnergyServiceProcessor = new LowEnergyServiceProcessor(m_pConfig, m_pLowEnergyController);
            QObject::connect(m_pConfig->m_pLowEnergyServiceProcessor, &LowEnergyServiceProcessor::serviceProcessingFailed,
                             [&](){
                m_pConfig->m_pLowEnergyServiceProcessor->deleteLater();
                m_pConfig->m_pLowEnergyServiceProcessor = nullptr;

                fail();
            });

            m_pConfig->m_pLowEnergyServiceProcessor->startProcessing();
        }
    } else {
        qInfo() << "Configured service not discovered.";
        fail();
    }
}

void LowEnergyDeviceProcessor::fail()
{
    if (m_pLowEnergyController->state() != QLowEnergyController::ControllerState::UnconnectedState) {
        qInfo() << "Disconnecting from device ...";
        m_pLowEnergyController->disconnectFromDevice();
    } else {
        if (m_LocalDevice.connectedDevices().contains(m_pConfig->m_MacAddress)) {
            qInfo() << "Erroneous device is still connected - unpairing ...";
            m_LocalDevice.requestPairing(m_pConfig->m_MacAddress, QBluetoothLocalDevice::Pairing::Unpaired);
        }
        emit deviceProcessingFailed();
    }
}
