#pragma once

#include <memory>
#include <QObject>
class QLowEnergyCharacteristic;
class QLowEnergyService;
class QByteArray;
struct ConfigurationEntry;
namespace lsl {
    class stream_outlet;
}

class CharacteristicChangeParser : public QObject
{
    Q_OBJECT

    std::shared_ptr<ConfigurationEntry> m_pConfig;
    std::shared_ptr<lsl::stream_outlet> m_pLSLOutletHeartrate;
    std::shared_ptr<lsl::stream_outlet> m_pLSLOutletRRIntervals;
    std::shared_ptr<lsl::stream_outlet> m_pLSLOutletBitalinoAnalogChannels;

    void parseHeartRateMeasurement(const QLowEnergyCharacteristic& characteristic, const QByteArray& value);
    void parseBitalinoDataOrStatusPacket(const QLowEnergyCharacteristic& characteristic, const QByteArray& value);

public:
    explicit CharacteristicChangeParser(std::shared_ptr<ConfigurationEntry> pConfig, QObject *parent = nullptr);
    void parse(const QLowEnergyCharacteristic& characteristic, const QByteArray& value);
    bool prepare(QLowEnergyService* pLowEnergyService);
};
