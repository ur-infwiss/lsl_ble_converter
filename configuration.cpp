#include "configuration.h"

#include <QSettings>
#include <QtDebug>
#include <vector>

CharacteristicChangeParser* ConfigurationEntry::parser()
{
    if (m_pCharacteristicChangeParser == nullptr) {
        m_pCharacteristicChangeParser = std::make_shared<CharacteristicChangeParser>(this->shared_from_this());
    }
    return m_pCharacteristicChangeParser.get();
}

std::vector<std::shared_ptr<ConfigurationEntry>> readConfigurationEntries(QSettings& settings)
{
    std::vector<std::shared_ptr<ConfigurationEntry>> result;

    for (QString groupName : settings.childGroups()) {
        settings.beginGroup(groupName);

        std::shared_ptr<ConfigurationEntry> pConfigurationEntry = std::make_shared<ConfigurationEntry>();

        pConfigurationEntry->m_ConfiguredName = groupName;
        pConfigurationEntry->m_MacAddress = QBluetoothAddress(settings.value("mac_address").toString());
        pConfigurationEntry->m_ServiceUuid = QBluetoothUuid(settings.value("service_uuid").toString());
		QString characteristicAsString = settings.value("characteristic").toString().toLower();
		if (characteristicAsString == "2a37" ||
			characteristicAsString == "heartratemeasurement") {
            pConfigurationEntry->m_CharacteristicUuid = ConfigurationEntry::characteristicHeartRateMeasurement();
		}
        else if (QBluetoothUuid(characteristicAsString) == ConfigurationEntry::characteristicBitalinoDataPackets()) {
            pConfigurationEntry->m_CharacteristicUuid = ConfigurationEntry::characteristicBitalinoDataPackets();
		}
		else {
			qCritical() << "Unexpected value in config file: characteristic " << characteristicAsString;
		}

        result.push_back(pConfigurationEntry);

        settings.endGroup();
    }

    return result;
}

QBluetoothUuid ConfigurationEntry::characteristicHeartRateMeasurement()
{
    return QBluetoothUuid(QBluetoothUuid::CharacteristicType::HeartRateMeasurement);
}

QBluetoothUuid ConfigurationEntry::characteristicBitalinoCommands()
{
    return QBluetoothUuid(QString("4051eb11-bf0a-4c74-8730-a48f4193fcea"));
}

QBluetoothUuid ConfigurationEntry::characteristicBitalinoDataPackets()
{
    return QBluetoothUuid(QString("40fdba6b-672e-47c4-808a-e529adff3633"));
}
