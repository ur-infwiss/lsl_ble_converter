#pragma once

#include <QObject>
struct ConfigurationEntry;
class QLowEnergyController;
class QLowEnergyService;

class LowEnergyServiceProcessor : public QObject
{
    Q_OBJECT

    std::shared_ptr<ConfigurationEntry> m_pConfig;
    QLowEnergyService* m_pLowEnergyService;

    void fail();
    void startUsing();

public:
    explicit LowEnergyServiceProcessor(std::shared_ptr<ConfigurationEntry> pConfig, QLowEnergyController* pQLowEnergyController, QObject *parent = nullptr);
    virtual void startProcessing();

signals:
    void serviceProcessingFailed();

public slots:
};
