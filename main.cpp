#include <QCoreApplication>
#include <QLocale>
#include <QDate>
#include <QSettings>
#include <QTextStream>
#include <QBluetoothLocalDevice>
#include <QBluetoothDeviceDiscoveryAgent>
#include <QtDebug>
#include <QTimer>
#include <QCommandLineParser>

#include <iostream>

#include "configuration.h"
#include "lowenergydeviceprocessor.h"

QString buildTimestamp() {
    QString build = QString("%1_%2")
            .arg(QLocale(QLocale::C).toDate(QString(__DATE__).simplified(), QLatin1String("MMM d yyyy")).toString("yyyy-MM-dd"))
            .arg(QString("%1%2-%3%4-%5%6")
                 .arg(__TIME__[0])
            .arg(__TIME__[1])
            .arg(__TIME__[3])
            .arg(__TIME__[4])
            .arg(__TIME__[6])
            .arg(__TIME__[7]));

    return build;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QCoreApplication::setApplicationName("LSL BLE Converter");
    QCoreApplication::setApplicationVersion(buildTimestamp());

    QCommandLineParser parser;
    parser.setApplicationDescription(QCoreApplication::applicationName());
    parser.addHelpOption();
    parser.addVersionOption();
    QCommandLineOption settingsFileOption(QStringList() << "c" << "config-file",
                                          "Path and filename of the config INI file to be used.",
                                          "path-to-file");
    settingsFileOption.setDefaultValue(QCoreApplication::applicationDirPath() + "/config.ini");
    parser.addOption(settingsFileOption);
    parser.process(a);
    QString settingsFile = parser.value(settingsFileOption);

    qInfo() << "Using configuration" << settingsFile << "...";
    QSettings settings(settingsFile, QSettings::IniFormat);
    auto configurationEntries = readConfigurationEntries(settings);
    qInfo() << configurationEntries.size() << "configuration entries read.";

    QBluetoothLocalDevice localDevice;
    for (const auto& pConfigEntry : configurationEntries) {
        localDevice.requestPairing(pConfigEntry->m_MacAddress, QBluetoothLocalDevice::Pairing::AuthorizedPaired);
    }

    QBluetoothDeviceDiscoveryAgent* deviceDiscoveryAgent = new QBluetoothDeviceDiscoveryAgent();

    QObject::connect(deviceDiscoveryAgent, &QBluetoothDeviceDiscoveryAgent::deviceDiscovered,
                     [&](const QBluetoothDeviceInfo& deviceInfo) {
        for (const auto& pConfigEntry : configurationEntries) {
            if (deviceInfo.address() == pConfigEntry->m_MacAddress) {
                pConfigEntry->m_DeviceName = deviceInfo.name();
                qInfo() << "Configured device discovered:'" << deviceInfo.name() << "'- address:" << deviceInfo.address().toString();

                if (pConfigEntry->m_pLowEnergyDeviceProcessor == nullptr) {
                    pConfigEntry->m_pLowEnergyDeviceProcessor = new LowEnergyDeviceProcessor(pConfigEntry, deviceInfo);

                    // immediately reconnect on fail
                    QObject::connect(pConfigEntry->m_pLowEnergyDeviceProcessor, &LowEnergyDeviceProcessor::deviceProcessingFailed,
                                     pConfigEntry->m_pLowEnergyDeviceProcessor, &LowEnergyDeviceProcessor::startProcessing);

                    // (first attempt to) connect to device
                    pConfigEntry->m_pLowEnergyDeviceProcessor->startProcessing();
                }
            }
        }
    });
    QObject::connect(deviceDiscoveryAgent, &QBluetoothDeviceDiscoveryAgent::canceled,
                     [&](){
        qInfo() << "Device discovery canceled.";
    });
    QObject::connect(deviceDiscoveryAgent, &QBluetoothDeviceDiscoveryAgent::finished,
                     [&](){
        qInfo() << "Device discovery finished.";
    });
    QObject::connect(deviceDiscoveryAgent, QOverload<QBluetoothDeviceDiscoveryAgent::Error>::of(&QBluetoothDeviceDiscoveryAgent::error),
                     [&](QBluetoothDeviceDiscoveryAgent::Error error){
        qInfo() << "QBluetoothDeviceDiscoveryAgent::Error" << error;
    });

    qInfo() << "Starting device discovery...";
    deviceDiscoveryAgent->setLowEnergyDiscoveryTimeout(0);
    deviceDiscoveryAgent->start(QBluetoothDeviceDiscoveryAgent::LowEnergyMethod);

    int exitCode = a.exec();
    qInfo() << "Exiting..."; // doesn't get called - TODO: FIXME
    return exitCode;
}
