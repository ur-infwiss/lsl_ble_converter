#pragma once

#include <QObject>
#include <QBluetoothLocalDevice>
class QBluetoothDeviceInfo;
class QLowEnergyController;
struct ConfigurationEntry;

class LowEnergyDeviceProcessor : public QObject
{
    Q_OBJECT

    std::shared_ptr<ConfigurationEntry> m_pConfig;
    QBluetoothLocalDevice m_LocalDevice;
    QLowEnergyController* m_pLowEnergyController;

public:
    explicit LowEnergyDeviceProcessor(std::shared_ptr<ConfigurationEntry> pConfig, const QBluetoothDeviceInfo& remoteDeviceInfo, QObject *parent = nullptr);
    virtual void startProcessing();

signals:
    void deviceProcessingFailed();

public slots:

private:
    void onServiceDiscoveryFinished();
    void fail();
    void startConnecting();
};
