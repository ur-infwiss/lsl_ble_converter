#pragma once

#include <QMetaEnum>

template<typename QEnum>
QString QtEnumToString(const QEnum value)
{
    return QString(QMetaEnum::fromType<QEnum>().valueToKey(value));
}
